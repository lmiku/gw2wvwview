﻿namespace Gw2WvwView
{
    partial class MapViewer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tEBPangloss = new System.Windows.Forms.Label();
            this.tEBVeloka = new System.Windows.Forms.Label();
            this.tEBMendons = new System.Windows.Forms.Label();
            this.tEBOverlook = new System.Windows.Forms.Label();
            this.tEBSpeldan = new System.Windows.Forms.Label();
            this.tEBOgrewatch = new System.Windows.Forms.Label();
            this.tEBAnzalias = new System.Windows.Forms.Label();
            this.tEBStonemist = new System.Windows.Forms.Label();
            this.tEBLangor = new System.Windows.Forms.Label();
            this.tEBValley = new System.Windows.Forms.Label();
            this.tEBBravost = new System.Windows.Forms.Label();
            this.tEBUmberglade = new System.Windows.Forms.Label();
            this.tEBDurios = new System.Windows.Forms.Label();
            this.tEBQuentin = new System.Windows.Forms.Label();
            this.tEBDanelon = new System.Windows.Forms.Label();
            this.tEBGolanta = new System.Windows.Forms.Label();
            this.tEBKlovan = new System.Windows.Forms.Label();
            this.tEBWildcreek = new System.Windows.Forms.Label();
            this.tEBRogues = new System.Windows.Forms.Label();
            this.tEBJerrifer = new System.Windows.Forms.Label();
            this.tEBLowlands = new System.Windows.Forms.Label();
            this.tEBAldons = new System.Windows.Forms.Label();
            this.tGreenSECamp = new System.Windows.Forms.Label();
            this.tGreenSWCamp = new System.Windows.Forms.Label();
            this.tGreenNECamp = new System.Windows.Forms.Label();
            this.tGreenNWCamp = new System.Windows.Forms.Label();
            this.tGreenNCamp = new System.Windows.Forms.Label();
            this.tGreenNETower = new System.Windows.Forms.Label();
            this.tGreenNWTower = new System.Windows.Forms.Label();
            this.tGreenGarrison = new System.Windows.Forms.Label();
            this.tGreenLake = new System.Windows.Forms.Label();
            this.tGreenBriar = new System.Windows.Forms.Label();
            this.tGreenSCamp = new System.Windows.Forms.Label();
            this.tGreenBay = new System.Windows.Forms.Label();
            this.tGreenHills = new System.Windows.Forms.Label();
            this.tBlueSECamp = new System.Windows.Forms.Label();
            this.tBlueSWCamp = new System.Windows.Forms.Label();
            this.tBlueNECamp = new System.Windows.Forms.Label();
            this.tBlueNWCamp = new System.Windows.Forms.Label();
            this.tBlueNCamp = new System.Windows.Forms.Label();
            this.tBlueNETower = new System.Windows.Forms.Label();
            this.tBlueNWTower = new System.Windows.Forms.Label();
            this.tBlueGarrison = new System.Windows.Forms.Label();
            this.tBlueLake = new System.Windows.Forms.Label();
            this.tBlueBriar = new System.Windows.Forms.Label();
            this.tBlueSCamp = new System.Windows.Forms.Label();
            this.tBlueBay = new System.Windows.Forms.Label();
            this.tBlueHills = new System.Windows.Forms.Label();
            this.tRedSECamp = new System.Windows.Forms.Label();
            this.tRedSWCamp = new System.Windows.Forms.Label();
            this.tRedNECamp = new System.Windows.Forms.Label();
            this.tRedNWCamp = new System.Windows.Forms.Label();
            this.tRedNCamp = new System.Windows.Forms.Label();
            this.tRedNETower = new System.Windows.Forms.Label();
            this.tRedNWTower = new System.Windows.Forms.Label();
            this.tRedGarrison = new System.Windows.Forms.Label();
            this.tRedLake = new System.Windows.Forms.Label();
            this.tRedBriar = new System.Windows.Forms.Label();
            this.tRedSCamp = new System.Windows.Forms.Label();
            this.tRedBay = new System.Windows.Forms.Label();
            this.tRedHills = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // tEBPangloss
            // 
            this.tEBPangloss.AutoSize = true;
            this.tEBPangloss.Location = new System.Drawing.Point(594, 511);
            this.tEBPangloss.Name = "tEBPangloss";
            this.tEBPangloss.Size = new System.Drawing.Size(41, 13);
            this.tEBPangloss.TabIndex = 239;
            this.tEBPangloss.Text = "0 m 0 s";
            // 
            // tEBVeloka
            // 
            this.tEBVeloka.AutoSize = true;
            this.tEBVeloka.Location = new System.Drawing.Point(569, 485);
            this.tEBVeloka.Name = "tEBVeloka";
            this.tEBVeloka.Size = new System.Drawing.Size(41, 13);
            this.tEBVeloka.TabIndex = 238;
            this.tEBVeloka.Text = "0 m 0 s";
            // 
            // tEBMendons
            // 
            this.tEBMendons.AutoSize = true;
            this.tEBMendons.Location = new System.Drawing.Point(494, 485);
            this.tEBMendons.Name = "tEBMendons";
            this.tEBMendons.Size = new System.Drawing.Size(41, 13);
            this.tEBMendons.TabIndex = 237;
            this.tEBMendons.Text = "0 m 0 s";
            // 
            // tEBOverlook
            // 
            this.tEBOverlook.AutoSize = true;
            this.tEBOverlook.Location = new System.Drawing.Point(529, 507);
            this.tEBOverlook.Name = "tEBOverlook";
            this.tEBOverlook.Size = new System.Drawing.Size(41, 13);
            this.tEBOverlook.TabIndex = 236;
            this.tEBOverlook.Text = "0 m 0 s";
            // 
            // tEBSpeldan
            // 
            this.tEBSpeldan.AutoSize = true;
            this.tEBSpeldan.Location = new System.Drawing.Point(439, 498);
            this.tEBSpeldan.Name = "tEBSpeldan";
            this.tEBSpeldan.Size = new System.Drawing.Size(41, 13);
            this.tEBSpeldan.TabIndex = 235;
            this.tEBSpeldan.Text = "0 m 0 s";
            // 
            // tEBOgrewatch
            // 
            this.tEBOgrewatch.AutoSize = true;
            this.tEBOgrewatch.Location = new System.Drawing.Point(553, 525);
            this.tEBOgrewatch.Name = "tEBOgrewatch";
            this.tEBOgrewatch.Size = new System.Drawing.Size(41, 13);
            this.tEBOgrewatch.TabIndex = 234;
            this.tEBOgrewatch.Text = "0 m 0 s";
            // 
            // tEBAnzalias
            // 
            this.tEBAnzalias.AutoSize = true;
            this.tEBAnzalias.Location = new System.Drawing.Point(478, 543);
            this.tEBAnzalias.Name = "tEBAnzalias";
            this.tEBAnzalias.Size = new System.Drawing.Size(41, 13);
            this.tEBAnzalias.TabIndex = 233;
            this.tEBAnzalias.Text = "0 m 0 s";
            // 
            // tEBStonemist
            // 
            this.tEBStonemist.AutoSize = true;
            this.tEBStonemist.Location = new System.Drawing.Point(516, 597);
            this.tEBStonemist.Name = "tEBStonemist";
            this.tEBStonemist.Size = new System.Drawing.Size(41, 13);
            this.tEBStonemist.TabIndex = 232;
            this.tEBStonemist.Text = "0 m 0 s";
            // 
            // tEBLangor
            // 
            this.tEBLangor.AutoSize = true;
            this.tEBLangor.Location = new System.Drawing.Point(616, 682);
            this.tEBLangor.Name = "tEBLangor";
            this.tEBLangor.Size = new System.Drawing.Size(41, 13);
            this.tEBLangor.TabIndex = 231;
            this.tEBLangor.Text = "0 m 0 s";
            // 
            // tEBValley
            // 
            this.tEBValley.AutoSize = true;
            this.tEBValley.Location = new System.Drawing.Point(604, 648);
            this.tEBValley.Name = "tEBValley";
            this.tEBValley.Size = new System.Drawing.Size(41, 13);
            this.tEBValley.TabIndex = 230;
            this.tEBValley.Text = "0 m 0 s";
            // 
            // tEBBravost
            // 
            this.tEBBravost.AutoSize = true;
            this.tEBBravost.Location = new System.Drawing.Point(636, 618);
            this.tEBBravost.Name = "tEBBravost";
            this.tEBBravost.Size = new System.Drawing.Size(41, 13);
            this.tEBBravost.TabIndex = 229;
            this.tEBBravost.Text = "0 m 0 s";
            // 
            // tEBUmberglade
            // 
            this.tEBUmberglade.AutoSize = true;
            this.tEBUmberglade.Location = new System.Drawing.Point(625, 582);
            this.tEBUmberglade.Name = "tEBUmberglade";
            this.tEBUmberglade.Size = new System.Drawing.Size(41, 13);
            this.tEBUmberglade.TabIndex = 228;
            this.tEBUmberglade.Text = "0 m 0 s";
            // 
            // tEBDurios
            // 
            this.tEBDurios.AutoSize = true;
            this.tEBDurios.Location = new System.Drawing.Point(569, 589);
            this.tEBDurios.Name = "tEBDurios";
            this.tEBDurios.Size = new System.Drawing.Size(41, 13);
            this.tEBDurios.TabIndex = 227;
            this.tEBDurios.Text = "0 m 0 s";
            // 
            // tEBQuentin
            // 
            this.tEBQuentin.AutoSize = true;
            this.tEBQuentin.Location = new System.Drawing.Point(543, 656);
            this.tEBQuentin.Name = "tEBQuentin";
            this.tEBQuentin.Size = new System.Drawing.Size(41, 13);
            this.tEBQuentin.TabIndex = 226;
            this.tEBQuentin.Text = "0 m 0 s";
            // 
            // tEBDanelon
            // 
            this.tEBDanelon.AutoSize = true;
            this.tEBDanelon.Location = new System.Drawing.Point(562, 695);
            this.tEBDanelon.Name = "tEBDanelon";
            this.tEBDanelon.Size = new System.Drawing.Size(41, 13);
            this.tEBDanelon.TabIndex = 225;
            this.tEBDanelon.Text = "0 m 0 s";
            // 
            // tEBGolanta
            // 
            this.tEBGolanta.AutoSize = true;
            this.tEBGolanta.Location = new System.Drawing.Point(486, 674);
            this.tEBGolanta.Name = "tEBGolanta";
            this.tEBGolanta.Size = new System.Drawing.Size(41, 13);
            this.tEBGolanta.TabIndex = 224;
            this.tEBGolanta.Text = "0 m 0 s";
            // 
            // tEBKlovan
            // 
            this.tEBKlovan.AutoSize = true;
            this.tEBKlovan.Location = new System.Drawing.Point(481, 642);
            this.tEBKlovan.Name = "tEBKlovan";
            this.tEBKlovan.Size = new System.Drawing.Size(41, 13);
            this.tEBKlovan.TabIndex = 223;
            this.tEBKlovan.Text = "0 m 0 s";
            // 
            // tEBWildcreek
            // 
            this.tEBWildcreek.AutoSize = true;
            this.tEBWildcreek.Location = new System.Drawing.Point(458, 597);
            this.tEBWildcreek.Name = "tEBWildcreek";
            this.tEBWildcreek.Size = new System.Drawing.Size(41, 13);
            this.tEBWildcreek.TabIndex = 222;
            this.tEBWildcreek.Text = "0 m 0 s";
            // 
            // tEBRogues
            // 
            this.tEBRogues.AutoSize = true;
            this.tEBRogues.Location = new System.Drawing.Point(414, 579);
            this.tEBRogues.Name = "tEBRogues";
            this.tEBRogues.Size = new System.Drawing.Size(41, 13);
            this.tEBRogues.TabIndex = 221;
            this.tEBRogues.Text = "0 m 0 s";
            // 
            // tEBJerrifer
            // 
            this.tEBJerrifer.AutoSize = true;
            this.tEBJerrifer.Location = new System.Drawing.Point(432, 679);
            this.tEBJerrifer.Name = "tEBJerrifer";
            this.tEBJerrifer.Size = new System.Drawing.Size(41, 13);
            this.tEBJerrifer.TabIndex = 220;
            this.tEBJerrifer.Text = "0 m 0 s";
            // 
            // tEBLowlands
            // 
            this.tEBLowlands.AutoSize = true;
            this.tEBLowlands.Location = new System.Drawing.Point(423, 650);
            this.tEBLowlands.Name = "tEBLowlands";
            this.tEBLowlands.Size = new System.Drawing.Size(41, 13);
            this.tEBLowlands.TabIndex = 219;
            this.tEBLowlands.Text = "0 m 0 s";
            // 
            // tEBAldons
            // 
            this.tEBAldons.AutoSize = true;
            this.tEBAldons.Location = new System.Drawing.Point(395, 615);
            this.tEBAldons.Name = "tEBAldons";
            this.tEBAldons.Size = new System.Drawing.Size(41, 13);
            this.tEBAldons.TabIndex = 218;
            this.tEBAldons.Text = "0 m 0 s";
            // 
            // tGreenSECamp
            // 
            this.tGreenSECamp.AutoSize = true;
            this.tGreenSECamp.Location = new System.Drawing.Point(244, 554);
            this.tGreenSECamp.Name = "tGreenSECamp";
            this.tGreenSECamp.Size = new System.Drawing.Size(41, 13);
            this.tGreenSECamp.TabIndex = 178;
            this.tGreenSECamp.Text = "0 m 0 s";
            // 
            // tGreenSWCamp
            // 
            this.tGreenSWCamp.AutoSize = true;
            this.tGreenSWCamp.Location = new System.Drawing.Point(55, 552);
            this.tGreenSWCamp.Name = "tGreenSWCamp";
            this.tGreenSWCamp.Size = new System.Drawing.Size(41, 13);
            this.tGreenSWCamp.TabIndex = 177;
            this.tGreenSWCamp.Text = "0 m 0 s";
            // 
            // tGreenNECamp
            // 
            this.tGreenNECamp.AutoSize = true;
            this.tGreenNECamp.Location = new System.Drawing.Point(242, 421);
            this.tGreenNECamp.Name = "tGreenNECamp";
            this.tGreenNECamp.Size = new System.Drawing.Size(41, 13);
            this.tGreenNECamp.TabIndex = 176;
            this.tGreenNECamp.Text = "0 m 0 s";
            // 
            // tGreenNWCamp
            // 
            this.tGreenNWCamp.AutoSize = true;
            this.tGreenNWCamp.Location = new System.Drawing.Point(54, 428);
            this.tGreenNWCamp.Name = "tGreenNWCamp";
            this.tGreenNWCamp.Size = new System.Drawing.Size(41, 13);
            this.tGreenNWCamp.TabIndex = 175;
            this.tGreenNWCamp.Text = "0 m 0 s";
            // 
            // tGreenNCamp
            // 
            this.tGreenNCamp.AutoSize = true;
            this.tGreenNCamp.Location = new System.Drawing.Point(146, 329);
            this.tGreenNCamp.Name = "tGreenNCamp";
            this.tGreenNCamp.Size = new System.Drawing.Size(41, 13);
            this.tGreenNCamp.TabIndex = 174;
            this.tGreenNCamp.Text = "0 m 0 s";
            // 
            // tGreenNETower
            // 
            this.tGreenNETower.AutoSize = true;
            this.tGreenNETower.Location = new System.Drawing.Point(204, 404);
            this.tGreenNETower.Name = "tGreenNETower";
            this.tGreenNETower.Size = new System.Drawing.Size(41, 13);
            this.tGreenNETower.TabIndex = 173;
            this.tGreenNETower.Text = "0 m 0 s";
            // 
            // tGreenNWTower
            // 
            this.tGreenNWTower.AutoSize = true;
            this.tGreenNWTower.Location = new System.Drawing.Point(93, 407);
            this.tGreenNWTower.Name = "tGreenNWTower";
            this.tGreenNWTower.Size = new System.Drawing.Size(41, 13);
            this.tGreenNWTower.TabIndex = 172;
            this.tGreenNWTower.Text = "0 m 0 s";
            // 
            // tGreenGarrison
            // 
            this.tGreenGarrison.AutoSize = true;
            this.tGreenGarrison.Location = new System.Drawing.Point(140, 445);
            this.tGreenGarrison.Name = "tGreenGarrison";
            this.tGreenGarrison.Size = new System.Drawing.Size(41, 13);
            this.tGreenGarrison.TabIndex = 171;
            this.tGreenGarrison.Text = "0 m 0 s";
            // 
            // tGreenLake
            // 
            this.tGreenLake.AutoSize = true;
            this.tGreenLake.Location = new System.Drawing.Point(185, 542);
            this.tGreenLake.Name = "tGreenLake";
            this.tGreenLake.Size = new System.Drawing.Size(41, 13);
            this.tGreenLake.TabIndex = 170;
            this.tGreenLake.Text = "0 m 0 s";
            // 
            // tGreenBriar
            // 
            this.tGreenBriar.AutoSize = true;
            this.tGreenBriar.Location = new System.Drawing.Point(106, 535);
            this.tGreenBriar.Name = "tGreenBriar";
            this.tGreenBriar.Size = new System.Drawing.Size(41, 13);
            this.tGreenBriar.TabIndex = 169;
            this.tGreenBriar.Text = "0 m 0 s";
            // 
            // tGreenSCamp
            // 
            this.tGreenSCamp.AutoSize = true;
            this.tGreenSCamp.Location = new System.Drawing.Point(144, 608);
            this.tGreenSCamp.Name = "tGreenSCamp";
            this.tGreenSCamp.Size = new System.Drawing.Size(41, 13);
            this.tGreenSCamp.TabIndex = 168;
            this.tGreenSCamp.Text = "0 m 0 s";
            // 
            // tGreenBay
            // 
            this.tGreenBay.AutoSize = true;
            this.tGreenBay.Location = new System.Drawing.Point(44, 495);
            this.tGreenBay.Name = "tGreenBay";
            this.tGreenBay.Size = new System.Drawing.Size(41, 13);
            this.tGreenBay.TabIndex = 167;
            this.tGreenBay.Text = "0 m 0 s";
            // 
            // tGreenHills
            // 
            this.tGreenHills.AutoSize = true;
            this.tGreenHills.Location = new System.Drawing.Point(255, 490);
            this.tGreenHills.Name = "tGreenHills";
            this.tGreenHills.Size = new System.Drawing.Size(41, 13);
            this.tGreenHills.TabIndex = 166;
            this.tGreenHills.Text = "0 m 0 s";
            // 
            // tBlueSECamp
            // 
            this.tBlueSECamp.AutoSize = true;
            this.tBlueSECamp.Location = new System.Drawing.Point(974, 489);
            this.tBlueSECamp.Name = "tBlueSECamp";
            this.tBlueSECamp.Size = new System.Drawing.Size(41, 13);
            this.tBlueSECamp.TabIndex = 165;
            this.tBlueSECamp.Text = "0 m 0 s";
            // 
            // tBlueSWCamp
            // 
            this.tBlueSWCamp.AutoSize = true;
            this.tBlueSWCamp.Location = new System.Drawing.Point(782, 484);
            this.tBlueSWCamp.Name = "tBlueSWCamp";
            this.tBlueSWCamp.Size = new System.Drawing.Size(41, 13);
            this.tBlueSWCamp.TabIndex = 164;
            this.tBlueSWCamp.Text = "0 m 0 s";
            // 
            // tBlueNECamp
            // 
            this.tBlueNECamp.AutoSize = true;
            this.tBlueNECamp.Location = new System.Drawing.Point(973, 358);
            this.tBlueNECamp.Name = "tBlueNECamp";
            this.tBlueNECamp.Size = new System.Drawing.Size(41, 13);
            this.tBlueNECamp.TabIndex = 163;
            this.tBlueNECamp.Text = "0 m 0 s";
            // 
            // tBlueNWCamp
            // 
            this.tBlueNWCamp.AutoSize = true;
            this.tBlueNWCamp.Location = new System.Drawing.Point(784, 364);
            this.tBlueNWCamp.Name = "tBlueNWCamp";
            this.tBlueNWCamp.Size = new System.Drawing.Size(41, 13);
            this.tBlueNWCamp.TabIndex = 162;
            this.tBlueNWCamp.Text = "0 m 0 s";
            // 
            // tBlueNCamp
            // 
            this.tBlueNCamp.AutoSize = true;
            this.tBlueNCamp.Location = new System.Drawing.Point(871, 265);
            this.tBlueNCamp.Name = "tBlueNCamp";
            this.tBlueNCamp.Size = new System.Drawing.Size(41, 13);
            this.tBlueNCamp.TabIndex = 161;
            this.tBlueNCamp.Text = "0 m 0 s";
            // 
            // tBlueNETower
            // 
            this.tBlueNETower.AutoSize = true;
            this.tBlueNETower.Location = new System.Drawing.Point(928, 342);
            this.tBlueNETower.Name = "tBlueNETower";
            this.tBlueNETower.Size = new System.Drawing.Size(41, 13);
            this.tBlueNETower.TabIndex = 160;
            this.tBlueNETower.Text = "0 m 0 s";
            // 
            // tBlueNWTower
            // 
            this.tBlueNWTower.AutoSize = true;
            this.tBlueNWTower.Location = new System.Drawing.Point(809, 346);
            this.tBlueNWTower.Name = "tBlueNWTower";
            this.tBlueNWTower.Size = new System.Drawing.Size(41, 13);
            this.tBlueNWTower.TabIndex = 159;
            this.tBlueNWTower.Text = "0 m 0 s";
            // 
            // tBlueGarrison
            // 
            this.tBlueGarrison.AutoSize = true;
            this.tBlueGarrison.Location = new System.Drawing.Point(865, 384);
            this.tBlueGarrison.Name = "tBlueGarrison";
            this.tBlueGarrison.Size = new System.Drawing.Size(41, 13);
            this.tBlueGarrison.TabIndex = 158;
            this.tBlueGarrison.Text = "0 m 0 s";
            // 
            // tBlueLake
            // 
            this.tBlueLake.AutoSize = true;
            this.tBlueLake.Location = new System.Drawing.Point(919, 478);
            this.tBlueLake.Name = "tBlueLake";
            this.tBlueLake.Size = new System.Drawing.Size(41, 13);
            this.tBlueLake.TabIndex = 157;
            this.tBlueLake.Text = "0 m 0 s";
            // 
            // tBlueBriar
            // 
            this.tBlueBriar.AutoSize = true;
            this.tBlueBriar.Location = new System.Drawing.Point(830, 472);
            this.tBlueBriar.Name = "tBlueBriar";
            this.tBlueBriar.Size = new System.Drawing.Size(41, 13);
            this.tBlueBriar.TabIndex = 156;
            this.tBlueBriar.Text = "0 m 0 s";
            // 
            // tBlueSCamp
            // 
            this.tBlueSCamp.AutoSize = true;
            this.tBlueSCamp.Location = new System.Drawing.Point(870, 544);
            this.tBlueSCamp.Name = "tBlueSCamp";
            this.tBlueSCamp.Size = new System.Drawing.Size(41, 13);
            this.tBlueSCamp.TabIndex = 155;
            this.tBlueSCamp.Text = "0 m 0 s";
            // 
            // tBlueBay
            // 
            this.tBlueBay.AutoSize = true;
            this.tBlueBay.Location = new System.Drawing.Point(766, 434);
            this.tBlueBay.Name = "tBlueBay";
            this.tBlueBay.Size = new System.Drawing.Size(41, 13);
            this.tBlueBay.TabIndex = 154;
            this.tBlueBay.Text = "0 m 0 s";
            // 
            // tBlueHills
            // 
            this.tBlueHills.AutoSize = true;
            this.tBlueHills.Location = new System.Drawing.Point(987, 425);
            this.tBlueHills.Name = "tBlueHills";
            this.tBlueHills.Size = new System.Drawing.Size(41, 13);
            this.tBlueHills.TabIndex = 153;
            this.tBlueHills.Text = "0 m 0 s";
            // 
            // tRedSECamp
            // 
            this.tRedSECamp.AutoSize = true;
            this.tRedSECamp.Location = new System.Drawing.Point(608, 299);
            this.tRedSECamp.Name = "tRedSECamp";
            this.tRedSECamp.Size = new System.Drawing.Size(41, 13);
            this.tRedSECamp.TabIndex = 152;
            this.tRedSECamp.Text = "0 m 0 s";
            // 
            // tRedSWCamp
            // 
            this.tRedSWCamp.AutoSize = true;
            this.tRedSWCamp.Location = new System.Drawing.Point(424, 301);
            this.tRedSWCamp.Name = "tRedSWCamp";
            this.tRedSWCamp.Size = new System.Drawing.Size(41, 13);
            this.tRedSWCamp.TabIndex = 151;
            this.tRedSWCamp.Text = "0 m 0 s";
            // 
            // tRedNECamp
            // 
            this.tRedNECamp.AutoSize = true;
            this.tRedNECamp.Location = new System.Drawing.Point(612, 172);
            this.tRedNECamp.Name = "tRedNECamp";
            this.tRedNECamp.Size = new System.Drawing.Size(41, 13);
            this.tRedNECamp.TabIndex = 150;
            this.tRedNECamp.Text = "0 m 0 s";
            // 
            // tRedNWCamp
            // 
            this.tRedNWCamp.AutoSize = true;
            this.tRedNWCamp.Location = new System.Drawing.Point(412, 178);
            this.tRedNWCamp.Name = "tRedNWCamp";
            this.tRedNWCamp.Size = new System.Drawing.Size(41, 13);
            this.tRedNWCamp.TabIndex = 149;
            this.tRedNWCamp.Text = "0 m 0 s";
            // 
            // tRedNCamp
            // 
            this.tRedNCamp.AutoSize = true;
            this.tRedNCamp.Location = new System.Drawing.Point(504, 83);
            this.tRedNCamp.Name = "tRedNCamp";
            this.tRedNCamp.Size = new System.Drawing.Size(41, 13);
            this.tRedNCamp.TabIndex = 148;
            this.tRedNCamp.Text = "0 m 0 s";
            // 
            // tRedNETower
            // 
            this.tRedNETower.AutoSize = true;
            this.tRedNETower.Location = new System.Drawing.Point(563, 155);
            this.tRedNETower.Name = "tRedNETower";
            this.tRedNETower.Size = new System.Drawing.Size(41, 13);
            this.tRedNETower.TabIndex = 147;
            this.tRedNETower.Text = "0 m 0 s";
            // 
            // tRedNWTower
            // 
            this.tRedNWTower.AutoSize = true;
            this.tRedNWTower.Location = new System.Drawing.Point(447, 160);
            this.tRedNWTower.Name = "tRedNWTower";
            this.tRedNWTower.Size = new System.Drawing.Size(41, 13);
            this.tRedNWTower.TabIndex = 146;
            this.tRedNWTower.Text = "0 m 0 s";
            // 
            // tRedGarrison
            // 
            this.tRedGarrison.AutoSize = true;
            this.tRedGarrison.Location = new System.Drawing.Point(502, 197);
            this.tRedGarrison.Name = "tRedGarrison";
            this.tRedGarrison.Size = new System.Drawing.Size(41, 13);
            this.tRedGarrison.TabIndex = 145;
            this.tRedGarrison.Text = "0 m 0 s";
            // 
            // tRedLake
            // 
            this.tRedLake.AutoSize = true;
            this.tRedLake.Location = new System.Drawing.Point(553, 289);
            this.tRedLake.Name = "tRedLake";
            this.tRedLake.Size = new System.Drawing.Size(41, 13);
            this.tRedLake.TabIndex = 144;
            this.tRedLake.Text = "0 m 0 s";
            // 
            // tRedBriar
            // 
            this.tRedBriar.AutoSize = true;
            this.tRedBriar.Location = new System.Drawing.Point(475, 283);
            this.tRedBriar.Name = "tRedBriar";
            this.tRedBriar.Size = new System.Drawing.Size(41, 13);
            this.tRedBriar.TabIndex = 143;
            this.tRedBriar.Text = "0 m 0 s";
            // 
            // tRedSCamp
            // 
            this.tRedSCamp.AutoSize = true;
            this.tRedSCamp.Location = new System.Drawing.Point(506, 354);
            this.tRedSCamp.Name = "tRedSCamp";
            this.tRedSCamp.Size = new System.Drawing.Size(41, 13);
            this.tRedSCamp.TabIndex = 142;
            this.tRedSCamp.Text = "0 m 0 s";
            // 
            // tRedBay
            // 
            this.tRedBay.AutoSize = true;
            this.tRedBay.Location = new System.Drawing.Point(403, 248);
            this.tRedBay.Name = "tRedBay";
            this.tRedBay.Size = new System.Drawing.Size(41, 13);
            this.tRedBay.TabIndex = 141;
            this.tRedBay.Text = "0 m 0 s";
            // 
            // tRedHills
            // 
            this.tRedHills.AutoSize = true;
            this.tRedHills.Location = new System.Drawing.Point(621, 240);
            this.tRedHills.Name = "tRedHills";
            this.tRedHills.Size = new System.Drawing.Size(41, 13);
            this.tRedHills.TabIndex = 140;
            this.tRedHills.Text = "0 m 0 s";
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // MapViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Gw2WvwView.Properties.Resources.wvw_allmaps_cr;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1064, 776);
            this.Controls.Add(this.tEBPangloss);
            this.Controls.Add(this.tEBVeloka);
            this.Controls.Add(this.tEBMendons);
            this.Controls.Add(this.tEBOverlook);
            this.Controls.Add(this.tEBSpeldan);
            this.Controls.Add(this.tEBOgrewatch);
            this.Controls.Add(this.tEBAnzalias);
            this.Controls.Add(this.tEBStonemist);
            this.Controls.Add(this.tEBLangor);
            this.Controls.Add(this.tEBValley);
            this.Controls.Add(this.tEBBravost);
            this.Controls.Add(this.tEBUmberglade);
            this.Controls.Add(this.tEBDurios);
            this.Controls.Add(this.tEBQuentin);
            this.Controls.Add(this.tEBDanelon);
            this.Controls.Add(this.tEBGolanta);
            this.Controls.Add(this.tEBKlovan);
            this.Controls.Add(this.tEBWildcreek);
            this.Controls.Add(this.tEBRogues);
            this.Controls.Add(this.tEBJerrifer);
            this.Controls.Add(this.tEBLowlands);
            this.Controls.Add(this.tEBAldons);
            this.Controls.Add(this.tGreenSECamp);
            this.Controls.Add(this.tGreenSWCamp);
            this.Controls.Add(this.tGreenNECamp);
            this.Controls.Add(this.tGreenNWCamp);
            this.Controls.Add(this.tGreenNCamp);
            this.Controls.Add(this.tGreenNETower);
            this.Controls.Add(this.tGreenNWTower);
            this.Controls.Add(this.tGreenGarrison);
            this.Controls.Add(this.tGreenLake);
            this.Controls.Add(this.tGreenBriar);
            this.Controls.Add(this.tGreenSCamp);
            this.Controls.Add(this.tGreenBay);
            this.Controls.Add(this.tGreenHills);
            this.Controls.Add(this.tBlueSECamp);
            this.Controls.Add(this.tBlueSWCamp);
            this.Controls.Add(this.tBlueNECamp);
            this.Controls.Add(this.tBlueNWCamp);
            this.Controls.Add(this.tBlueNCamp);
            this.Controls.Add(this.tBlueNETower);
            this.Controls.Add(this.tBlueNWTower);
            this.Controls.Add(this.tBlueGarrison);
            this.Controls.Add(this.tBlueLake);
            this.Controls.Add(this.tBlueBriar);
            this.Controls.Add(this.tBlueSCamp);
            this.Controls.Add(this.tBlueBay);
            this.Controls.Add(this.tBlueHills);
            this.Controls.Add(this.tRedSECamp);
            this.Controls.Add(this.tRedSWCamp);
            this.Controls.Add(this.tRedNECamp);
            this.Controls.Add(this.tRedNWCamp);
            this.Controls.Add(this.tRedNCamp);
            this.Controls.Add(this.tRedNETower);
            this.Controls.Add(this.tRedNWTower);
            this.Controls.Add(this.tRedGarrison);
            this.Controls.Add(this.tRedLake);
            this.Controls.Add(this.tRedBriar);
            this.Controls.Add(this.tRedSCamp);
            this.Controls.Add(this.tRedBay);
            this.Controls.Add(this.tRedHills);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "MapViewer";
            this.Text = "MapViewer";
            this.Load += new System.EventHandler(this.MapViewer_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label tEBPangloss;
        private System.Windows.Forms.Label tEBVeloka;
        private System.Windows.Forms.Label tEBMendons;
        private System.Windows.Forms.Label tEBOverlook;
        private System.Windows.Forms.Label tEBSpeldan;
        private System.Windows.Forms.Label tEBOgrewatch;
        private System.Windows.Forms.Label tEBAnzalias;
        private System.Windows.Forms.Label tEBStonemist;
        private System.Windows.Forms.Label tEBLangor;
        private System.Windows.Forms.Label tEBValley;
        private System.Windows.Forms.Label tEBBravost;
        private System.Windows.Forms.Label tEBUmberglade;
        private System.Windows.Forms.Label tEBDurios;
        private System.Windows.Forms.Label tEBQuentin;
        private System.Windows.Forms.Label tEBDanelon;
        private System.Windows.Forms.Label tEBGolanta;
        private System.Windows.Forms.Label tEBKlovan;
        private System.Windows.Forms.Label tEBWildcreek;
        private System.Windows.Forms.Label tEBRogues;
        private System.Windows.Forms.Label tEBJerrifer;
        private System.Windows.Forms.Label tEBLowlands;
        private System.Windows.Forms.Label tEBAldons;
        private System.Windows.Forms.Label tGreenSECamp;
        private System.Windows.Forms.Label tGreenSWCamp;
        private System.Windows.Forms.Label tGreenNECamp;
        private System.Windows.Forms.Label tGreenNWCamp;
        private System.Windows.Forms.Label tGreenNCamp;
        private System.Windows.Forms.Label tGreenNETower;
        private System.Windows.Forms.Label tGreenNWTower;
        private System.Windows.Forms.Label tGreenGarrison;
        private System.Windows.Forms.Label tGreenLake;
        private System.Windows.Forms.Label tGreenBriar;
        private System.Windows.Forms.Label tGreenSCamp;
        private System.Windows.Forms.Label tGreenBay;
        private System.Windows.Forms.Label tGreenHills;
        private System.Windows.Forms.Label tBlueSECamp;
        private System.Windows.Forms.Label tBlueSWCamp;
        private System.Windows.Forms.Label tBlueNECamp;
        private System.Windows.Forms.Label tBlueNWCamp;
        private System.Windows.Forms.Label tBlueNCamp;
        private System.Windows.Forms.Label tBlueNETower;
        private System.Windows.Forms.Label tBlueNWTower;
        private System.Windows.Forms.Label tBlueGarrison;
        private System.Windows.Forms.Label tBlueLake;
        private System.Windows.Forms.Label tBlueBriar;
        private System.Windows.Forms.Label tBlueSCamp;
        private System.Windows.Forms.Label tBlueBay;
        private System.Windows.Forms.Label tBlueHills;
        private System.Windows.Forms.Label tRedSECamp;
        private System.Windows.Forms.Label tRedSWCamp;
        private System.Windows.Forms.Label tRedNECamp;
        private System.Windows.Forms.Label tRedNWCamp;
        private System.Windows.Forms.Label tRedNCamp;
        private System.Windows.Forms.Label tRedNETower;
        private System.Windows.Forms.Label tRedNWTower;
        private System.Windows.Forms.Label tRedGarrison;
        private System.Windows.Forms.Label tRedLake;
        private System.Windows.Forms.Label tRedBriar;
        private System.Windows.Forms.Label tRedSCamp;
        private System.Windows.Forms.Label tRedBay;
        private System.Windows.Forms.Label tRedHills;
        private System.Windows.Forms.Timer timer1;
    }
}