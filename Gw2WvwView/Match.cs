﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Match.cs">
//   This product is licensed under the creative commons Attribution-NonCommercial-ShareAlike 3.0 Unported (CC BY-NC-SA 3.0) as defined on the following page: http://creativecommons.org/licenses/by-nc-sa/3.0/
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RestSharp;

namespace Gw2WvwView
{

    public class Match
    {
        public string WvwMatchId
        { get; set; }
        public int RedWorldId
        { get; set; }
        public int BlueWorldId
        { get; set; }
        public int GreenWorldId
        { get; set; }

    }
}
