﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ControlHelper.cs">
//   This product is licensed under the creative commons Attribution-NonCommercial-ShareAlike 3.0 Unported (CC BY-NC-SA 3.0) as defined on the following page: http://creativecommons.org/licenses/by-nc-sa/3.0/
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace Gw2WvwView
{
    public class ControlHelper
    {
        private Color Red = Color.OrangeRed;
        private Color Blue = Color.CornflowerBlue;
        private Color Green = Color.LightGreen;

        public ControlHelper()
        {

        }

        public string findLabelById(int id)
        {
            #region "objectives.."
            switch (id)
            {
                case 1:
                    return "tEBOverlook";
                case 2:
                    return "tEBValley";
                case 3:
                    return "tEBLowlands";
                case 4:
                    return "tEBGolanta";
                case 5:
                    return "tEBPangloss";
                case 6:
                    return "tEBSpeldan";
                case 7:
                    return "tEBDanelon";
                case 8:
                    return "tEBUmberglade";
                case 9:
                    return "tEBStonemist";
                case 10:
                    return "tEBRogues";
                case 11:
                    return "tEBAldons";
                case 12:
                    return "tEBWildcreek";
                case 13:
                    return "tEBJerrifer";
                case 14:
                    return "tEBKlovan";
                case 15:
                    return "tEBLangor";
                case 16:
                    return "tEBQuentin";
                case 17:
                    return "tEBMendons";
                case 18:
                    return "tEBAnzalias";
                case 19:
                    return "tEBOgrewatch";
                case 20:
                    return "tEBVeloka";
                case 21:
                    return "tEBDurios";
                case 22:
                    return "tEBBravost";
                case 23:
                    return "tBlueGarrison";
                case 24:
                    return "tBlueSCamp";
                case 25:
                    return "tBlueBriar";
                case 26:
                    return "tBlueLake";
                case 27:
                    return "tBlueBay";
                case 28:
                    return "tBlueNETower";
                case 29:
                    return "tBlueNCamp";
                case 30:
                    return "tBlueNWTower";
                case 31:
                    return "tBlueHills";
                case 32:
                    return "tRedHills";
                case 33:
                    return "tRedBay";
                case 34:
                    return "tRedSCamp";
                case 35:
                    return "tRedBriar";
                case 36:
                    return "tRedLake";
                case 37:
                    return "tRedGarrison";
                case 38:
                    return "tRedNWTower";
                case 39:
                    return "tRedNCamp";
                case 40:
                    return "tRedNETower";
                case 41:
                    return "tGreenHills";
                case 42:
                    return "tGreenLake";
                case 43:
                    return "tGreenSCamp";
                case 44:
                    return "tGreenBay";
                case 45:
                    return "tGreenBriar";
                case 46:
                    return "tGreenGarrison";
                case 47:
                    return "tGreenNWTower";
                case 48:
                    return "tGreenNWCamp";
                case 49:
                    return "tGreenSWCamp";
                case 50:
                    return "tRedSECamp";
                case 51:
                    return "tRedNECamp";
                case 52:
                    return "tRedNWCamp";
                case 53:
                    return "tRedSWCamp";
                case 54:
                    return "tGreenNECamp";
                case 55:
                    return "tGreenSECamp";
                case 56:
                    return "tGreenNCamp";
                case 57:
                    return "tGreenNETower";
                case 58:
                    return "tBlueNWCamp";
                case 59:
                    return "tBlueSWCamp";
                case 60:
                    return "tBlueNECamp";
                case 61:
                    return "tBlueSECamp";
            }
            #endregion
            return "";
        }

        public string findLabelByIdGeneralized(int id)
        {
            #region "objectives.."
            switch (id)
            {
                case 1:
                    return "tEBOverlook";
                case 2:
                    return "tEBValley";
                case 3:
                    return "tEBLowlands";
                case 4:
                    return "tEBGolanta";
                case 5:
                    return "tEBPangloss";
                case 6:
                    return "tEBSpeldan";
                case 7:
                    return "tEBDanelon";
                case 8:
                    return "tEBUmberglade";
                case 9:
                    return "tEBStonemist";
                case 10:
                    return "tEBRogues";
                case 11:
                    return "tEBAldons";
                case 12:
                    return "tEBWildcreek";
                case 13:
                    return "tEBJerrifer";
                case 14:
                    return "tEBKlovan";
                case 15:
                    return "tEBLangor";
                case 16:
                    return "tEBQuentin";
                case 17:
                    return "tEBMendons";
                case 18:
                    return "tEBAnzalias";
                case 19:
                    return "tEBOgrewatch";
                case 20:
                    return "tEBVeloka";
                case 21:
                    return "tEBDurios";
                case 22:
                    return "tEBBravost";
                case 23:
                    return "tGarrison";
                case 24:
                    return "tSCamp";
                case 25:
                    return "tBriar";
                case 26:
                    return "tLake";
                case 27:
                    return "tBay";
                case 28:
                    return "tNETower";
                case 29:
                    return "tNCamp";
                case 30:
                    return "tNWTower";
                case 31:
                    return "tHills";
                case 32:
                    return "tHills";
                case 33:
                    return "tBay";
                case 34:
                    return "tSCamp";
                case 35:
                    return "tBriar";
                case 36:
                    return "tLake";
                case 37:
                    return "tGarrison";
                case 38:
                    return "tNWTower";
                case 39:
                    return "tNCamp";
                case 40:
                    return "tNETower";
                case 41:
                    return "tHills";
                case 42:
                    return "tLake";
                case 43:
                    return "tSCamp";
                case 44:
                    return "tBay";
                case 45:
                    return "tBriar";
                case 46:
                    return "tGarrison";
                case 47:
                    return "tNWTower";
                case 48:
                    return "tNWCamp";
                case 49:
                    return "tSWCamp";
                case 50:
                    return "tSECamp";
                case 51:
                    return "tNECamp";
                case 52:
                    return "tNWCamp";
                case 53:
                    return "tSWCamp";
                case 54:
                    return "tNECamp";
                case 55:
                    return "tSECamp";
                case 56:
                    return "tNCamp";
                case 57:
                    return "tNETower";
                case 58:
                    return "tNWCamp";
                case 59:
                    return "tSWCamp";
                case 60:
                    return "tNECamp";
                case 61:
                    return "tSECamp";
            }
            #endregion
            return "";
        }

        public Color getOwnerColor(string owner)
        {
            if (owner == "Red")
            {
                return this.Red;
            }
            else if (owner == "Blue")
            {
                return this.Blue;
            }
            else if (owner == "Green")
            {
                return this.Green;
            }
            else
            {
                return Color.White;
            }
        }
    }
}
