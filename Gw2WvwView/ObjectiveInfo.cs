﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ObjectiveInfo.cs">
//   This product is licensed under the creative commons Attribution-NonCommercial-ShareAlike 3.0 Unported (CC BY-NC-SA 3.0) as defined on the following page: http://creativecommons.org/licenses/by-nc-sa/3.0/
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Gw2WvwView
{
    public class ObjectiveInfo
    {
        public int Id
        { get; set; }
        public string Name
        { get; set; }
    }
}
