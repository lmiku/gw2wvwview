﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="APILoadHelper.cs">
//   This product is licensed under the creative commons Attribution-NonCommercial-ShareAlike 3.0 Unported (CC BY-NC-SA 3.0) as defined on the following page: http://creativecommons.org/licenses/by-nc-sa/3.0/
// </copyright>
// --------------------------------------------------------------------------------------------------------------------


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RestSharp;

namespace Gw2WvwView
{
    public class APILoadHelper
    {
        public APILoadHelper()
        {

        }

        public List<Match> loadMatches(IRestClient client)
        {
            List<Match> matches = new List<Match>();

            IRestRequest request = new RestRequest("wvw/matches.json", Method.GET);
            IRestResponse<MatchList> response = client.Execute<MatchList>(request);
            foreach (Match m in response.Data.WvwMatches)
            {
                matches.Add(m);
            }
            matches = matches.OrderBy(o => o.WvwMatchId).ToList();
            return matches;
        }

        public MatchDetails loadMatchdetails(IRestClient client, string matchId)
        {
            IRestRequest request = new RestRequest("wvw/match_details.json", Method.GET);
            request.AddParameter("match_id", matchId);

            IRestResponse<MatchDetails> response = client.Execute<MatchDetails>(request);

            return response.Data;

        }

        public List<WorldName> loadWorldNames(IRestClient client)
        {
            List<WorldName> worldNames = new List<WorldName>();

            IRestRequest request = new RestRequest("world_names.json");
            IRestResponse<List<WorldName>> response = client.Execute<List<WorldName>>(request);

            foreach (WorldName item in response.Data)
            {
                worldNames.Add(item);
            }
            worldNames = worldNames.OrderBy(o => o.Name).ToList();
            return worldNames;
        }

        public  List<ObjectiveInfo> loadObjectiveInfo(IRestClient client)
        {
            List<ObjectiveInfo> objectiveInfoSmall = new List<ObjectiveInfo>();
            IRestRequest request = new RestRequest("wvw/objective_names.json");
            IRestResponse<List<ObjectiveInfo>> response = client.Execute<List<ObjectiveInfo>>(request);
            foreach (ObjectiveInfo item in response.Data)
            {
                objectiveInfoSmall.Add(item);
            }
            return objectiveInfoSmall;
        }


    }
}
