﻿namespace Gw2WvwView
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.bViewEB = new System.Windows.Forms.Button();
            this.bViewRed = new System.Windows.Forms.Button();
            this.bViewBlue = new System.Windows.Forms.Button();
            this.bViewGreen = new System.Windows.Forms.Button();
            this.bViewAll = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(12, 12);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(316, 21);
            this.comboBox1.TabIndex = 3;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(334, 10);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "Start";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_2);
            // 
            // bViewEB
            // 
            this.bViewEB.Enabled = false;
            this.bViewEB.Location = new System.Drawing.Point(126, 49);
            this.bViewEB.Name = "bViewEB";
            this.bViewEB.Size = new System.Drawing.Size(75, 23);
            this.bViewEB.TabIndex = 136;
            this.bViewEB.Text = "View";
            this.bViewEB.UseVisualStyleBackColor = true;
            this.bViewEB.Click += new System.EventHandler(this.button2_Click);
            // 
            // bViewRed
            // 
            this.bViewRed.Enabled = false;
            this.bViewRed.Location = new System.Drawing.Point(347, 49);
            this.bViewRed.Name = "bViewRed";
            this.bViewRed.Size = new System.Drawing.Size(75, 23);
            this.bViewRed.TabIndex = 137;
            this.bViewRed.Text = "View";
            this.bViewRed.UseVisualStyleBackColor = true;
            this.bViewRed.Click += new System.EventHandler(this.button3_Click);
            // 
            // bViewBlue
            // 
            this.bViewBlue.Enabled = false;
            this.bViewBlue.Location = new System.Drawing.Point(347, 78);
            this.bViewBlue.Name = "bViewBlue";
            this.bViewBlue.Size = new System.Drawing.Size(75, 23);
            this.bViewBlue.TabIndex = 138;
            this.bViewBlue.Text = "View";
            this.bViewBlue.UseVisualStyleBackColor = true;
            this.bViewBlue.Click += new System.EventHandler(this.button4_Click);
            // 
            // bViewGreen
            // 
            this.bViewGreen.Enabled = false;
            this.bViewGreen.Location = new System.Drawing.Point(347, 107);
            this.bViewGreen.Name = "bViewGreen";
            this.bViewGreen.Size = new System.Drawing.Size(75, 23);
            this.bViewGreen.TabIndex = 139;
            this.bViewGreen.Text = "View";
            this.bViewGreen.UseVisualStyleBackColor = true;
            this.bViewGreen.Click += new System.EventHandler(this.button5_Click);
            // 
            // bViewAll
            // 
            this.bViewAll.Enabled = false;
            this.bViewAll.Location = new System.Drawing.Point(126, 78);
            this.bViewAll.Name = "bViewAll";
            this.bViewAll.Size = new System.Drawing.Size(75, 23);
            this.bViewAll.TabIndex = 140;
            this.bViewAll.Text = "View All";
            this.bViewAll.UseVisualStyleBackColor = true;
            this.bViewAll.Click += new System.EventHandler(this.bViewAll_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(207, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 13);
            this.label1.TabIndex = 141;
            this.label1.Text = "((Red World))";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(207, 83);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 13);
            this.label2.TabIndex = 142;
            this.label2.Text = "((Blue World))";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(207, 112);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 13);
            this.label3.TabIndex = 143;
            this.label3.Text = "((Green World))";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 54);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(108, 13);
            this.label4.TabIndex = 144;
            this.label4.Text = "Eternal Battlegrounds";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 83);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 13);
            this.label5.TabIndex = 145;
            this.label5.Text = "All Maps";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(473, 144);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.bViewAll);
            this.Controls.Add(this.bViewGreen);
            this.Controls.Add(this.bViewBlue);
            this.Controls.Add(this.bViewRed);
            this.Controls.Add(this.bViewEB);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.comboBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Gw2WvwView - Objective Tracker by Quecc";
            this.Activated += new System.EventHandler(this.Form1_Activated);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Shown += new System.EventHandler(this.Form1_Shown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button bViewEB;
        private System.Windows.Forms.Button bViewRed;
        private System.Windows.Forms.Button bViewBlue;
        private System.Windows.Forms.Button bViewGreen;
        private System.Windows.Forms.Button bViewAll;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;

    }
}

