﻿namespace Gw2WvwView
{
    partial class EBViewer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.tEBPangloss = new System.Windows.Forms.Label();
            this.tEBVeloka = new System.Windows.Forms.Label();
            this.tEBMendons = new System.Windows.Forms.Label();
            this.tEBOverlook = new System.Windows.Forms.Label();
            this.tEBSpeldan = new System.Windows.Forms.Label();
            this.tEBOgrewatch = new System.Windows.Forms.Label();
            this.tEBAnzalias = new System.Windows.Forms.Label();
            this.tEBStonemist = new System.Windows.Forms.Label();
            this.tEBLangor = new System.Windows.Forms.Label();
            this.tEBValley = new System.Windows.Forms.Label();
            this.tEBBravost = new System.Windows.Forms.Label();
            this.tEBUmberglade = new System.Windows.Forms.Label();
            this.tEBDurios = new System.Windows.Forms.Label();
            this.tEBQuentin = new System.Windows.Forms.Label();
            this.tEBDanelon = new System.Windows.Forms.Label();
            this.tEBGolanta = new System.Windows.Forms.Label();
            this.tEBKlovan = new System.Windows.Forms.Label();
            this.tEBWildcreek = new System.Windows.Forms.Label();
            this.tEBRogues = new System.Windows.Forms.Label();
            this.tEBJerrifer = new System.Windows.Forms.Label();
            this.tEBLowlands = new System.Windows.Forms.Label();
            this.tEBAldons = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // tEBPangloss
            // 
            this.tEBPangloss.AutoSize = true;
            this.tEBPangloss.Location = new System.Drawing.Point(459, 182);
            this.tEBPangloss.Name = "tEBPangloss";
            this.tEBPangloss.Size = new System.Drawing.Size(41, 13);
            this.tEBPangloss.TabIndex = 158;
            this.tEBPangloss.Text = "0 m 0 s";
            // 
            // tEBVeloka
            // 
            this.tEBVeloka.AutoSize = true;
            this.tEBVeloka.Location = new System.Drawing.Point(414, 134);
            this.tEBVeloka.Name = "tEBVeloka";
            this.tEBVeloka.Size = new System.Drawing.Size(41, 13);
            this.tEBVeloka.TabIndex = 157;
            this.tEBVeloka.Text = "0 m 0 s";
            // 
            // tEBMendons
            // 
            this.tEBMendons.AutoSize = true;
            this.tEBMendons.Location = new System.Drawing.Point(243, 140);
            this.tEBMendons.Name = "tEBMendons";
            this.tEBMendons.Size = new System.Drawing.Size(41, 13);
            this.tEBMendons.TabIndex = 156;
            this.tEBMendons.Text = "0 m 0 s";
            // 
            // tEBOverlook
            // 
            this.tEBOverlook.AutoSize = true;
            this.tEBOverlook.Location = new System.Drawing.Point(354, 169);
            this.tEBOverlook.Name = "tEBOverlook";
            this.tEBOverlook.Size = new System.Drawing.Size(41, 13);
            this.tEBOverlook.TabIndex = 155;
            this.tEBOverlook.Text = "0 m 0 s";
            // 
            // tEBSpeldan
            // 
            this.tEBSpeldan.AutoSize = true;
            this.tEBSpeldan.Location = new System.Drawing.Point(157, 150);
            this.tEBSpeldan.Name = "tEBSpeldan";
            this.tEBSpeldan.Size = new System.Drawing.Size(41, 13);
            this.tEBSpeldan.TabIndex = 154;
            this.tEBSpeldan.Text = "0 m 0 s";
            // 
            // tEBOgrewatch
            // 
            this.tEBOgrewatch.AutoSize = true;
            this.tEBOgrewatch.Location = new System.Drawing.Point(403, 209);
            this.tEBOgrewatch.Name = "tEBOgrewatch";
            this.tEBOgrewatch.Size = new System.Drawing.Size(41, 13);
            this.tEBOgrewatch.TabIndex = 153;
            this.tEBOgrewatch.Text = "0 m 0 s";
            // 
            // tEBAnzalias
            // 
            this.tEBAnzalias.AutoSize = true;
            this.tEBAnzalias.Location = new System.Drawing.Point(227, 257);
            this.tEBAnzalias.Name = "tEBAnzalias";
            this.tEBAnzalias.Size = new System.Drawing.Size(41, 13);
            this.tEBAnzalias.TabIndex = 152;
            this.tEBAnzalias.Text = "0 m 0 s";
            // 
            // tEBStonemist
            // 
            this.tEBStonemist.AutoSize = true;
            this.tEBStonemist.Location = new System.Drawing.Point(319, 358);
            this.tEBStonemist.Name = "tEBStonemist";
            this.tEBStonemist.Size = new System.Drawing.Size(41, 13);
            this.tEBStonemist.TabIndex = 151;
            this.tEBStonemist.Text = "0 m 0 s";
            // 
            // tEBLangor
            // 
            this.tEBLangor.AutoSize = true;
            this.tEBLangor.Location = new System.Drawing.Point(495, 546);
            this.tEBLangor.Name = "tEBLangor";
            this.tEBLangor.Size = new System.Drawing.Size(41, 13);
            this.tEBLangor.TabIndex = 150;
            this.tEBLangor.Text = "0 m 0 s";
            // 
            // tEBValley
            // 
            this.tEBValley.AutoSize = true;
            this.tEBValley.Location = new System.Drawing.Point(505, 469);
            this.tEBValley.Name = "tEBValley";
            this.tEBValley.Size = new System.Drawing.Size(41, 13);
            this.tEBValley.TabIndex = 149;
            this.tEBValley.Text = "0 m 0 s";
            // 
            // tEBBravost
            // 
            this.tEBBravost.AutoSize = true;
            this.tEBBravost.Location = new System.Drawing.Point(561, 402);
            this.tEBBravost.Name = "tEBBravost";
            this.tEBBravost.Size = new System.Drawing.Size(41, 13);
            this.tEBBravost.TabIndex = 148;
            this.tEBBravost.Text = "0 m 0 s";
            // 
            // tEBUmberglade
            // 
            this.tEBUmberglade.AutoSize = true;
            this.tEBUmberglade.Location = new System.Drawing.Point(522, 338);
            this.tEBUmberglade.Name = "tEBUmberglade";
            this.tEBUmberglade.Size = new System.Drawing.Size(41, 13);
            this.tEBUmberglade.TabIndex = 147;
            this.tEBUmberglade.Text = "0 m 0 s";
            // 
            // tEBDurios
            // 
            this.tEBDurios.AutoSize = true;
            this.tEBDurios.Location = new System.Drawing.Point(432, 352);
            this.tEBDurios.Name = "tEBDurios";
            this.tEBDurios.Size = new System.Drawing.Size(41, 13);
            this.tEBDurios.TabIndex = 146;
            this.tEBDurios.Text = "0 m 0 s";
            // 
            // tEBQuentin
            // 
            this.tEBQuentin.AutoSize = true;
            this.tEBQuentin.Location = new System.Drawing.Point(368, 490);
            this.tEBQuentin.Name = "tEBQuentin";
            this.tEBQuentin.Size = new System.Drawing.Size(41, 13);
            this.tEBQuentin.TabIndex = 145;
            this.tEBQuentin.Text = "0 m 0 s";
            // 
            // tEBDanelon
            // 
            this.tEBDanelon.AutoSize = true;
            this.tEBDanelon.Location = new System.Drawing.Point(408, 561);
            this.tEBDanelon.Name = "tEBDanelon";
            this.tEBDanelon.Size = new System.Drawing.Size(41, 13);
            this.tEBDanelon.TabIndex = 144;
            this.tEBDanelon.Text = "0 m 0 s";
            // 
            // tEBGolanta
            // 
            this.tEBGolanta.AutoSize = true;
            this.tEBGolanta.Location = new System.Drawing.Point(227, 530);
            this.tEBGolanta.Name = "tEBGolanta";
            this.tEBGolanta.Size = new System.Drawing.Size(41, 13);
            this.tEBGolanta.TabIndex = 143;
            this.tEBGolanta.Text = "0 m 0 s";
            // 
            // tEBKlovan
            // 
            this.tEBKlovan.AutoSize = true;
            this.tEBKlovan.Location = new System.Drawing.Point(229, 460);
            this.tEBKlovan.Name = "tEBKlovan";
            this.tEBKlovan.Size = new System.Drawing.Size(41, 13);
            this.tEBKlovan.TabIndex = 142;
            this.tEBKlovan.Text = "0 m 0 s";
            // 
            // tEBWildcreek
            // 
            this.tEBWildcreek.AutoSize = true;
            this.tEBWildcreek.Location = new System.Drawing.Point(171, 368);
            this.tEBWildcreek.Name = "tEBWildcreek";
            this.tEBWildcreek.Size = new System.Drawing.Size(41, 13);
            this.tEBWildcreek.TabIndex = 141;
            this.tEBWildcreek.Text = "0 m 0 s";
            // 
            // tEBRogues
            // 
            this.tEBRogues.AutoSize = true;
            this.tEBRogues.Location = new System.Drawing.Point(104, 333);
            this.tEBRogues.Name = "tEBRogues";
            this.tEBRogues.Size = new System.Drawing.Size(41, 13);
            this.tEBRogues.TabIndex = 140;
            this.tEBRogues.Text = "0 m 0 s";
            // 
            // tEBJerrifer
            // 
            this.tEBJerrifer.AutoSize = true;
            this.tEBJerrifer.Location = new System.Drawing.Point(150, 528);
            this.tEBJerrifer.Name = "tEBJerrifer";
            this.tEBJerrifer.Size = new System.Drawing.Size(41, 13);
            this.tEBJerrifer.TabIndex = 139;
            this.tEBJerrifer.Text = "0 m 0 s";
            // 
            // tEBLowlands
            // 
            this.tEBLowlands.AutoSize = true;
            this.tEBLowlands.Location = new System.Drawing.Point(108, 472);
            this.tEBLowlands.Name = "tEBLowlands";
            this.tEBLowlands.Size = new System.Drawing.Size(41, 13);
            this.tEBLowlands.TabIndex = 138;
            this.tEBLowlands.Text = "0 m 0 s";
            // 
            // tEBAldons
            // 
            this.tEBAldons.AutoSize = true;
            this.tEBAldons.Location = new System.Drawing.Point(70, 405);
            this.tEBAldons.Name = "tEBAldons";
            this.tEBAldons.Size = new System.Drawing.Size(41, 13);
            this.tEBAldons.TabIndex = 137;
            this.tEBAldons.Text = "0 m 0 s";
            // 
            // EBViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Gw2WvwView.Properties.Resources.eb_uncut_cr;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(634, 612);
            this.Controls.Add(this.tEBPangloss);
            this.Controls.Add(this.tEBVeloka);
            this.Controls.Add(this.tEBMendons);
            this.Controls.Add(this.tEBOverlook);
            this.Controls.Add(this.tEBSpeldan);
            this.Controls.Add(this.tEBOgrewatch);
            this.Controls.Add(this.tEBAnzalias);
            this.Controls.Add(this.tEBStonemist);
            this.Controls.Add(this.tEBLangor);
            this.Controls.Add(this.tEBValley);
            this.Controls.Add(this.tEBBravost);
            this.Controls.Add(this.tEBUmberglade);
            this.Controls.Add(this.tEBDurios);
            this.Controls.Add(this.tEBQuentin);
            this.Controls.Add(this.tEBDanelon);
            this.Controls.Add(this.tEBGolanta);
            this.Controls.Add(this.tEBKlovan);
            this.Controls.Add(this.tEBWildcreek);
            this.Controls.Add(this.tEBRogues);
            this.Controls.Add(this.tEBJerrifer);
            this.Controls.Add(this.tEBLowlands);
            this.Controls.Add(this.tEBAldons);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "EBViewer";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "EBViewer";
            this.Load += new System.EventHandler(this.EBViewer_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label tEBPangloss;
        private System.Windows.Forms.Label tEBVeloka;
        private System.Windows.Forms.Label tEBMendons;
        private System.Windows.Forms.Label tEBOverlook;
        private System.Windows.Forms.Label tEBSpeldan;
        private System.Windows.Forms.Label tEBOgrewatch;
        private System.Windows.Forms.Label tEBAnzalias;
        private System.Windows.Forms.Label tEBStonemist;
        private System.Windows.Forms.Label tEBLangor;
        private System.Windows.Forms.Label tEBValley;
        private System.Windows.Forms.Label tEBBravost;
        private System.Windows.Forms.Label tEBUmberglade;
        private System.Windows.Forms.Label tEBDurios;
        private System.Windows.Forms.Label tEBQuentin;
        private System.Windows.Forms.Label tEBDanelon;
        private System.Windows.Forms.Label tEBGolanta;
        private System.Windows.Forms.Label tEBKlovan;
        private System.Windows.Forms.Label tEBWildcreek;
        private System.Windows.Forms.Label tEBRogues;
        private System.Windows.Forms.Label tEBJerrifer;
        private System.Windows.Forms.Label tEBLowlands;
        private System.Windows.Forms.Label tEBAldons;
    }
}