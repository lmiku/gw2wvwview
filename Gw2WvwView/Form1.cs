﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Form1.cs">
//   This product is licensed under the creative commons Attribution-NonCommercial-ShareAlike 3.0 Unported (CC BY-NC-SA 3.0) as defined on the following page: http://creativecommons.org/licenses/by-nc-sa/3.0/
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

using RestSharp;

namespace Gw2WvwView
{
    public partial class Form1 : Form
    {

        private IRestClient client;
        private List<WorldName> worldNames;
        private List<Match> matches;
        private APILoadHelper api;
        private ControlHelper control;
        private bool runningViewer;

        private EBViewer ebViewer;
        private BLViewer redBLViewer;
        private BLViewer blueBLViewer;
        private BLViewer greenBLViewer;
        private MapViewer mapViewer;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
           
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }


        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click_2(object sender, EventArgs e)
        {
            if (runningViewer)
            {
                comboBox1.Enabled = true;
                button1.Text = "Start";
                if(ebViewer != default(EBViewer))
                {
                    ebViewer.Close();
                }
                if (blueBLViewer != default(BLViewer))
                {
                    blueBLViewer.Close();
                }
                if (redBLViewer != default(BLViewer))
                {
                    redBLViewer.Close();
                }
                if (greenBLViewer != default(BLViewer))
                {
                    greenBLViewer.Close();
                }
                bViewEB.Enabled = false;
                bViewRed.Enabled = false;
                bViewBlue.Enabled = false;
                bViewGreen.Enabled = false;
                bViewAll.Enabled = false;
            }
            else
            {
                comboBox1.Enabled = false;
                button1.Text = "Stop";
                bViewEB.Enabled = true;
                bViewRed.Enabled = true;
                bViewBlue.Enabled = true;
                bViewGreen.Enabled = true;
                bViewAll.Enabled = true;
                label1.Text = worldNames.Find(x => x.Id == matches.ElementAt(comboBox1.SelectedIndex).RedWorldId).Name;
                label2.Text = worldNames.Find(x => x.Id == matches.ElementAt(comboBox1.SelectedIndex).BlueWorldId).Name;
                label3.Text = worldNames.Find(x => x.Id == matches.ElementAt(comboBox1.SelectedIndex).GreenWorldId).Name;
            }
            runningViewer = !runningViewer;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
      
        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            client = new RestClient("https://api.guildwars2.com/v1");
            api = new APILoadHelper();
            control = new ControlHelper();

            worldNames = api.loadWorldNames(client);
            matches = api.loadMatches(client);

            foreach (Match m in matches)
            {
                comboBox1.Items.Add(((m.WvwMatchId.ElementAt(0) == '1') ? "NA T" : "EU T")
                    + m.WvwMatchId.ElementAt(2) + " "
                    + worldNames.Find(x => x.Id == m.RedWorldId).Name + " - " +
                    worldNames.Find(x => x.Id == m.BlueWorldId).Name + " - " +
                    worldNames.Find(x => x.Id == m.GreenWorldId).Name);
            }
            comboBox1.SelectedIndex = 0;
            runningViewer = false;
        }

        private void Form1_Activated(object sender, EventArgs e)
        {

        }

        private void tRedBay_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            ebViewer = new EBViewer(this.matches.ElementAt(comboBox1.SelectedIndex).WvwMatchId);
            bViewEB.Enabled = false;
            bViewRed.Enabled = false;
            bViewBlue.Enabled = false;
            bViewGreen.Enabled = false;
            ebViewer.ShowDialog();
            bViewEB.Enabled = true;
            bViewRed.Enabled = true;
            bViewBlue.Enabled = true;
            bViewGreen.Enabled = true;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            bViewEB.Enabled = false;
            bViewRed.Enabled = false;
            bViewBlue.Enabled = false;
            bViewGreen.Enabled = false;
            redBLViewer = new BLViewer(this.matches.ElementAt(comboBox1.SelectedIndex).WvwMatchId, 0, "Red Borderlands");
            
            redBLViewer.ShowDialog();
            bViewEB.Enabled = true;
            bViewRed.Enabled = true;
            bViewBlue.Enabled = true;
            bViewGreen.Enabled = true;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            bViewEB.Enabled = false;
            bViewRed.Enabled = false;
            bViewBlue.Enabled = false;
            bViewGreen.Enabled = false;
            blueBLViewer = new BLViewer(this.matches.ElementAt(comboBox1.SelectedIndex).WvwMatchId, 2, "Blue Borderlands");
            
            blueBLViewer.ShowDialog();
            bViewEB.Enabled = true;
            bViewRed.Enabled = true;
            bViewBlue.Enabled = true;
            bViewGreen.Enabled = true;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            bViewEB.Enabled = false;
            bViewRed.Enabled = false;
            bViewBlue.Enabled = false;
            bViewGreen.Enabled = false;
            greenBLViewer = new BLViewer(this.matches.ElementAt(comboBox1.SelectedIndex).WvwMatchId, 1, "Green Borderlands");
            
            greenBLViewer.ShowDialog();
            bViewEB.Enabled = true;
            bViewRed.Enabled = true;
            bViewBlue.Enabled = true;
            bViewGreen.Enabled = true;
        }

        private void bViewAll_Click(object sender, EventArgs e)
        {
            bViewEB.Enabled = false;
            bViewRed.Enabled = false;
            bViewBlue.Enabled = false;
            bViewGreen.Enabled = false;
            mapViewer = new MapViewer(this.matches.ElementAt(comboBox1.SelectedIndex).WvwMatchId);

            mapViewer.ShowDialog();
            bViewEB.Enabled = true;
            bViewRed.Enabled = true;
            bViewBlue.Enabled = true;
            bViewGreen.Enabled = true;
        }
    }


}
