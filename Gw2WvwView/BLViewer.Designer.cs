﻿namespace Gw2WvwView
{
    partial class BLViewer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.tSECamp = new System.Windows.Forms.Label();
            this.tSWCamp = new System.Windows.Forms.Label();
            this.tNECamp = new System.Windows.Forms.Label();
            this.tNWCamp = new System.Windows.Forms.Label();
            this.tNCamp = new System.Windows.Forms.Label();
            this.tNETower = new System.Windows.Forms.Label();
            this.tNWTower = new System.Windows.Forms.Label();
            this.tGarrison = new System.Windows.Forms.Label();
            this.tLake = new System.Windows.Forms.Label();
            this.tBriar = new System.Windows.Forms.Label();
            this.tSCamp = new System.Windows.Forms.Label();
            this.tBay = new System.Windows.Forms.Label();
            this.tHills = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // tSECamp
            // 
            this.tSECamp.AutoSize = true;
            this.tSECamp.Location = new System.Drawing.Point(424, 472);
            this.tSECamp.Name = "tSECamp";
            this.tSECamp.Size = new System.Drawing.Size(41, 13);
            this.tSECamp.TabIndex = 105;
            this.tSECamp.Text = "0 m 0 s";
            // 
            // tSWCamp
            // 
            this.tSWCamp.AutoSize = true;
            this.tSWCamp.Location = new System.Drawing.Point(49, 464);
            this.tSWCamp.Name = "tSWCamp";
            this.tSWCamp.Size = new System.Drawing.Size(41, 13);
            this.tSWCamp.TabIndex = 104;
            this.tSWCamp.Text = "0 m 0 s";
            // 
            // tNECamp
            // 
            this.tNECamp.AutoSize = true;
            this.tNECamp.Location = new System.Drawing.Point(433, 209);
            this.tNECamp.Name = "tNECamp";
            this.tNECamp.Size = new System.Drawing.Size(41, 13);
            this.tNECamp.TabIndex = 103;
            this.tNECamp.Text = "0 m 0 s";
            // 
            // tNWCamp
            // 
            this.tNWCamp.AutoSize = true;
            this.tNWCamp.Location = new System.Drawing.Point(42, 218);
            this.tNWCamp.Name = "tNWCamp";
            this.tNWCamp.Size = new System.Drawing.Size(41, 13);
            this.tNWCamp.TabIndex = 102;
            this.tNWCamp.Text = "0 m 0 s";
            // 
            // tNCamp
            // 
            this.tNCamp.AutoSize = true;
            this.tNCamp.Location = new System.Drawing.Point(225, 28);
            this.tNCamp.Name = "tNCamp";
            this.tNCamp.Size = new System.Drawing.Size(41, 13);
            this.tNCamp.TabIndex = 101;
            this.tNCamp.Text = "0 m 0 s";
            // 
            // tNETower
            // 
            this.tNETower.AutoSize = true;
            this.tNETower.Location = new System.Drawing.Point(346, 186);
            this.tNETower.Name = "tNETower";
            this.tNETower.Size = new System.Drawing.Size(41, 13);
            this.tNETower.TabIndex = 100;
            this.tNETower.Text = "0 m 0 s";
            // 
            // tNWTower
            // 
            this.tNWTower.AutoSize = true;
            this.tNWTower.Location = new System.Drawing.Point(95, 194);
            this.tNWTower.Name = "tNWTower";
            this.tNWTower.Size = new System.Drawing.Size(41, 13);
            this.tNWTower.TabIndex = 99;
            this.tNWTower.Text = "0 m 0 s";
            // 
            // tGarrison
            // 
            this.tGarrison.AutoSize = true;
            this.tGarrison.Location = new System.Drawing.Point(213, 264);
            this.tGarrison.Name = "tGarrison";
            this.tGarrison.Size = new System.Drawing.Size(41, 13);
            this.tGarrison.TabIndex = 98;
            this.tGarrison.Text = "0 m 0 s";
            // 
            // tLake
            // 
            this.tLake.AutoSize = true;
            this.tLake.Location = new System.Drawing.Point(326, 452);
            this.tLake.Name = "tLake";
            this.tLake.Size = new System.Drawing.Size(41, 13);
            this.tLake.TabIndex = 97;
            this.tLake.Text = "0 m 0 s";
            // 
            // tBriar
            // 
            this.tBriar.AutoSize = true;
            this.tBriar.Location = new System.Drawing.Point(135, 439);
            this.tBriar.Name = "tBriar";
            this.tBriar.Size = new System.Drawing.Size(41, 13);
            this.tBriar.TabIndex = 96;
            this.tBriar.Text = "0 m 0 s";
            // 
            // tSCamp
            // 
            this.tSCamp.AutoSize = true;
            this.tSCamp.Location = new System.Drawing.Point(223, 579);
            this.tSCamp.Name = "tSCamp";
            this.tSCamp.Size = new System.Drawing.Size(41, 13);
            this.tSCamp.TabIndex = 95;
            this.tSCamp.Text = "0 m 0 s";
            // 
            // tBay
            // 
            this.tBay.AutoSize = true;
            this.tBay.Location = new System.Drawing.Point(12, 364);
            this.tBay.Name = "tBay";
            this.tBay.Size = new System.Drawing.Size(41, 13);
            this.tBay.TabIndex = 94;
            this.tBay.Text = "0 m 0 s";
            // 
            // tHills
            // 
            this.tHills.AutoSize = true;
            this.tHills.Location = new System.Drawing.Point(459, 351);
            this.tHills.Name = "tHills";
            this.tHills.Size = new System.Drawing.Size(41, 13);
            this.tHills.TabIndex = 93;
            this.tHills.Text = "0 m 0 s";
            // 
            // BLViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Gw2WvwView.Properties.Resources.bl_uncut_cr;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(514, 626);
            this.Controls.Add(this.tSECamp);
            this.Controls.Add(this.tSWCamp);
            this.Controls.Add(this.tNECamp);
            this.Controls.Add(this.tNWCamp);
            this.Controls.Add(this.tNCamp);
            this.Controls.Add(this.tNETower);
            this.Controls.Add(this.tNWTower);
            this.Controls.Add(this.tGarrison);
            this.Controls.Add(this.tLake);
            this.Controls.Add(this.tBriar);
            this.Controls.Add(this.tSCamp);
            this.Controls.Add(this.tBay);
            this.Controls.Add(this.tHills);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "BLViewer";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "BLViewer";
            this.Load += new System.EventHandler(this.BLViewer_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label tSECamp;
        private System.Windows.Forms.Label tSWCamp;
        private System.Windows.Forms.Label tNECamp;
        private System.Windows.Forms.Label tNWCamp;
        private System.Windows.Forms.Label tNCamp;
        private System.Windows.Forms.Label tNETower;
        private System.Windows.Forms.Label tNWTower;
        private System.Windows.Forms.Label tGarrison;
        private System.Windows.Forms.Label tLake;
        private System.Windows.Forms.Label tBriar;
        private System.Windows.Forms.Label tSCamp;
        private System.Windows.Forms.Label tBay;
        private System.Windows.Forms.Label tHills;
    }
}