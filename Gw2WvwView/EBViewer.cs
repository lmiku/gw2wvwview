﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EBViewer.cs">
//   This product is licensed under the creative commons Attribution-NonCommercial-ShareAlike 3.0 Unported (CC BY-NC-SA 3.0) as defined on the following page: http://creativecommons.org/licenses/by-nc-sa/3.0/
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using RestSharp;

namespace Gw2WvwView
{
    public partial class EBViewer : Form
    {
        private IRestClient client;
        private MatchDetails matchDetails;
        private List<ObjectiveInfo> objectiveInfos;
        private string matchId;
        private APILoadHelper api;
        private ControlHelper control;

        public EBViewer(string match)
        {
            InitializeComponent();
            matchId = match;
        }

        private void EBViewer_Load(object sender, EventArgs e)
        {
            client = new RestClient("https://api.guildwars2.com/v1");
            matchDetails = null;
            api = new APILoadHelper();
            control = new ControlHelper();
            timer1.Enabled = true;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            fillEBStatus();
        }

        private void fillEBStatus()
        {

            if (matchDetails == null)
            {
                matchDetails = api.loadMatchdetails(client, matchId);
            }
            MatchDetails newMatchDetails = api.loadMatchdetails(client,matchId);

            //just EB
            int i = 3;
                //each objective
                for (int j = 0; j < this.matchDetails.Maps.ElementAt(i).Objectives.Count; j++)
                {
                    //is the buff down? 
                    if ((DateTime.Now - matchDetails.Maps.ElementAt(i).Objectives.ElementAt(j).LastFlipped) > new TimeSpan(0, 5, 0))
                    {
                        //reset timer
                        matchDetails.Maps.ElementAt(i).Objectives.ElementAt(j).LastFlipped = default(DateTime);
                    }
                    //find label to write to
                    Control[] label = this.Controls.Find(control.findLabelById(matchDetails.Maps.ElementAt(i).Objectives.ElementAt(j).Id), true);
                    //write info
                    //no buff?
                    if (matchDetails.Maps.ElementAt(i).Objectives.ElementAt(j).LastFlipped == default(DateTime))
                    {
                        ((Label)label[0]).Text = "0 s";
                    }
                    else
                    {
                        int secondsLeft = (int)(300 - Math.Round((DateTime.Now - matchDetails.Maps.ElementAt(i).Objectives.ElementAt(j).LastFlipped).TotalSeconds));
                        ((Label)label[0]).Text = (secondsLeft >= 60) ? (secondsLeft / 60 + " m " + secondsLeft % 60 + " s") : secondsLeft + " s";
                    }
                    //show owner
                    ((Label)label[0]).BackColor = control.getOwnerColor(matchDetails.Maps.ElementAt(i).Objectives.ElementAt(j).Owner);

                    //did owner change? (updating here so its 1 update behind
                    if (newMatchDetails.Maps.ElementAt(i).Objectives.ElementAt(j).Owner != matchDetails.Maps.ElementAt(i).Objectives.ElementAt(j).Owner)
                    {
                        //update timer
                        newMatchDetails.Maps.ElementAt(i).Objectives.ElementAt(j).LastFlipped = DateTime.Now;
                    }
                    else
                    {
                        //use old timer
                        newMatchDetails.Maps.ElementAt(i).Objectives.ElementAt(j).LastFlipped = matchDetails.Maps.ElementAt(i).Objectives.ElementAt(j).LastFlipped;
                    }
                }
            
            matchDetails = newMatchDetails;
        }
    }
}
