﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Gw2WvwView
{
    public class MapDetails
    {
        public string Type
        { get; set; }
        public List<int> Scores
        { get; set; }
        public List<Objective> Objectives
        { get; set; }
    }
}
